describe('dropdownController', function() {
    var scope, $http, $httpBackend, createController, url;
    var $window, $state;

    beforeEach(module('myApp'));


    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();
        OrganizationalCtnl = $controller('OrganizationalCtnl', {
            $scope: scope
        });
    }));

    it('Organization length', function() {
        expect(scope.org.length).toEqual(3);
    });

    it('Suborg length', function() {
        expect(scope.suborg.length).toEqual(2);
    });

    it('salesteam length', function() {
        expect(scope.salesTeam.length).toEqual(2);
    });

    it('salesteamgroup length', function() {
        expect(scope.salesTeam.length).toEqual(2);
    });


    it('if organization value is 0  and division is Marketing checking the messages output', function() {
        scope.Organization = {"id" : 0, "division" : "Marketing"};

        scope.GetSelectedOrganization();
        expect(scope.orgid).toEqual(0);
        expect(scope.divisionname).toEqual("Marketing");
        expect(scope.marketingshow).toEqual("false");
        expect(scope.marketingmessage).toEqual("true");
    });

    it('if organization value is 1  and division is Sales checking the messages output', function() {
        scope.Organization = {"id" : 1, "division" : "Sales"};

        scope.GetSelectedOrganization();
        expect(scope.orgid).toEqual(1);
        expect(scope.divisionname).toEqual("Sales");
        expect(scope.test).toEqual("true");
        expect(scope.marketingshow).toEqual("false");
        expect(scope.salesmessahe).toEqual("true");
    });

    it('if organization value is 2  and division is CustomerCare checking the messages output', function() {
        scope.Organization = {"id" : 2, "division" : "CustomerCare"};

        scope.GetSelectedOrganization();
        expect(scope.orgid).toEqual(2);
        expect(scope.divisionname).toEqual("CustomerCare");
        expect(scope.orgshow).toEqual("false");
        expect(scope.customercareMessage).toEqual("true");
    });



});