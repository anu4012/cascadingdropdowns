/**
 * Created by Anu on 2/6/2016.
 */
angular.module('myApp',[])
    .controller("OrganizationalCtnl", ["$scope", function($scope){

        $scope.org = [ //First array for organization control
            {id:"0", division :"Marketing" },
            {id:"1", division:"Sales"},
            {id:"2", division:"CustomerCare"}
        ];

        $scope.suborg = [ ["Marketing team A","Marketing team B"], //Suborg array for organization control for 2th drop down selection
            ["Territories"],

        ];

        $scope.salesTeam =   [{id:"0", territory :"Territory A" }, // sales team array to achieve the org sequence for 3th drop down selection
            {id:"1", territory:"Territory B"}
        ];




        $scope.salesTeamgroup = { 0: ["Sales Team A1" , "Sales Team A2"], // final sales Team  array to achieve the org sequence for 4 th drop down selection
            1: ["Sales Team B1" , "Sales Team B2"]

        };
        //control variables created which are used to show and not to show Html Elements
        $scope.orgshow = "true";
        $scope.customercareMessage = "false";
        $scope.marketingmessage = "false";
        $scope.marketingshow = "true"
        $scope.salesmessahe="false";
        $scope.test= "false";
        $scope.GetSelectedOrganization= function () {// function to populate 2nd dropdown (sub org) based on 1st dropdown selection

            //console.log($scope.Organization.id);
            $scope.orgid = $scope.Organization.id;
            $scope.divisionname = $scope.Organization.division;
            var key = $scope.Organization.id;
            var newSelection = $scope.suborg[key];
            $scope.newsubselction = newSelection;

            if($scope.divisionname == "CustomerCare"){
                $scope.orgshow = "false";
                $scope.customercareMessage = "true";

            }else{
                $scope.orgshow = "true";
                $scope.customercareMessage = "false";
            }

            if($scope.divisionname == "Marketing"){
                $scope.marketingshow="false";
                $scope.marketingmessage = "true";
            }else{
                $scope.marketingshow="true";
                $scope.marketingmessage = "false";
            }

            if($scope.divisionname == "Sales"){
                $scope.test= "true";
                $scope.marketingshow="false";
                $scope.salesmessahe = "true";
            }else{
                $scope.test= "false";
                $scope.marketingshow="false";
                $scope.salesmessahe = "false";
            }

        };

        $scope.Showterritories = function () {
            var key = document.getElementById("org").value;
            if( ($scope.suborg[key] == "Territories") && (($scope.org[document.getElementById("org").value].division) == "Sales" ) ){
                return true;
            }
        };


    }]);
